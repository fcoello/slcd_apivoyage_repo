package apba.teleport.slcdc.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CreateVoyageRestDTO {
	
	private Date dateIni;
	private Date dateEnd;
	private String user;
	private String pass;
	private Integer days;
	private String envTeleport;
	private String envSlcd;
	
	public CreateVoyageRestDTO() {
		super();
		this.dateIni = null;
		this.dateEnd = null;
		this.user = null;
		this.pass = null;
		this.days = null;
		this.envTeleport = null;
		this.envSlcd = null;
	}
	
	public CreateVoyageRestDTO(Date dateIni, Date dateFin, String user, String pass,
							   Integer days, String envTeleport, String envSlcd) {
		super();
		this.dateIni = dateIni;
		this.dateEnd = dateEnd;
		this.user = user;
		this.pass = pass;
		this.days = days;
		this.envTeleport = envTeleport;
		this.envSlcd = envSlcd;
	}
}
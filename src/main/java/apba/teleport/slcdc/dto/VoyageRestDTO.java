package apba.teleport.slcdc.dto;

import lombok.Data;

import java.util.Date;

@Data
public class VoyageRestDTO {
	
	private Long id;
	private String number;
	private String type;
	private Long shipImo;
	private String consolidator;
	private Date dateTimeETA;
	private Date initOpsDateTime;
	private Date endOpsDateTime;
	private String voyageStatus;
	private String shippingCompany;
	
	public VoyageRestDTO() {
		super();
		this.id = null;
		this.number = null;
		this.type = null;
		this.shipImo = null;
		this.consolidator = null;
		this.dateTimeETA = null;
		this.initOpsDateTime = null;
		this.endOpsDateTime = null;
		this.voyageStatus = null;
		this.shippingCompany = null;
	}

}
package apba.teleport.slcdc.dto;

import lombok.Data;

@Data
public class ApiUserRestDTO
{
    private String username;
    private String password;
}


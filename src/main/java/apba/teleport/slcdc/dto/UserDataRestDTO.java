package apba.teleport.slcdc.dto;

import lombok.Data;

@Data
public class UserDataRestDTO {

	String userName;
    String token;
    String teleportSession;
    String password;
}

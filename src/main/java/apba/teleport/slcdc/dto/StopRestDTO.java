package apba.teleport.slcdc.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class StopRestDTO {

    private Long id;

    private Long version;

    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date stopEta;

    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date stopEtd;

    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date stopAta;
	
    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date stopAtd;
	
    private Long visitId;
	
    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date visitEta;
	
    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date visitEtd;
	
    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date visitAta;
	
    @DateTimeFormat(pattern = "yyyyMMddHHmm")
    private Date visitAtd;

    private String visitStatusId;

	private String stopStatus;

    private Long visitNumber;

    private Long shipId;

    private String callSign;

    private String flag;

	private String mmsi;

    private String shipName;

    private Long shipImo;

    private String nextPortCode;

    private Long agentId;

    private String agentName;

    private String agentCif;

	private String locationName;

	private String locationCode;

	private Long locationId;

}

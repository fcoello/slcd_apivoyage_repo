package apba.teleport.slcdc.utils;

import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.ResourceBundle;

@Data
public class Properties {

	static final ResourceBundle CONSTANTES_BUNDLE;

	HashMap<String, String> vars = new HashMap<>();

	static {
		CONSTANTES_BUNDLE = ResourceBundle.getBundle("application");
	}

	public enum EnumSistema {
		TELEPORT,
		SLCD;

		public String getData() {
			return CONSTANTES_BUNDLE.getString(this.name());
		}

	}

	public enum EnumEntorno {
		ENTORNO_TELEPORT,
		ENTORNO_SLCD;

		public String getData() {
			return CONSTANTES_BUNDLE.getString(this.name());
		}

	}

	public enum EnumMetodo {
		URL_POSTUSERDATAREST("URL_POSTUSERDATAREST"),
		URL_POSTTOKENUSER("URL_POSTTOKENUSER"),
		URL_GETSTOPS("URL_GETSTOPS"),
		URL_FINDVOYAGE("URL_FINDVOYAGE"),
		URL_POSTNEWVOYAGE("URL_POSTNEWVOYAGE");

		private String metodo = "";

		private EnumMetodo(String metodo){
			this.metodo = metodo;
		}

		public String getMetodo() {
			return metodo;
		}

		public String getData() {
			return CONSTANTES_BUNDLE.getString(this.name());
		}

	}

	public enum EnumParam {
		USER_URL_POSTUSERDATAREST("USER_URL_POSTUSERDATAREST"),
		PASS_URL_POSTUSERDATAREST("PASS_URL_POSTUSERDATAREST"),
		USER_URL_POSTTOKENUSER("USER_URL_POSTTOKENUSER"),
		PASS_URL_POSTTOKENUSER("PASS_URL_POSTTOKENUSER");

		private String param = "";

		private EnumParam(String param){
			this.param = param;
		}

		public String getParam() {
			return param;
		}

		public String getData() {
			return CONSTANTES_BUNDLE.getString(this.name());
		}

	}

	public String getData(EnumSistema system, EnumEntorno entorno, EnumMetodo method) {
		String cadena = system.getData() + "." +  entorno.getData() + "." + method.getData();
		return CONSTANTES_BUNDLE.getString(cadena);
	}

	public String getData(EnumSistema system, String entorno, EnumMetodo method) {
		String cadena = system.getData() + "." +  entorno + "." + method.getData();
		return CONSTANTES_BUNDLE.getString(cadena);
	}

	public String getData(EnumSistema system, EnumEntorno entorno, EnumMetodo method, EnumParam param) {
		String cadena = system.getData() + "." +  entorno.getData() + "." + method.getData() + "." + param.getData() ;
		return CONSTANTES_BUNDLE.getString(cadena);
	}

	public String getData(EnumSistema system, String entorno, EnumMetodo method, EnumParam param) {
		String cadena = system.getData() + "." +  entorno + "." + method.getData() + "." + param.getData() ;
		return CONSTANTES_BUNDLE.getString(cadena);
	}

	public void calculateVars(String envTeleport, String envSlcd)
	{
		String urlPostUserDataRest = "";
		String userUrlPostUserDataRest = "";
		String passUrlPostUserDataRest = "";
		String urlGetStops = "";
		String urlPostTokenUser = "";
		String userUrlPostTokenUser = "";
		String passUrlPostTokenUser = "";
		String urlFindVoyage = "";
		String urlPostNewVoyage = "";

		if(StringUtils.isEmpty(envTeleport)) {
			urlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					Properties.EnumEntorno.ENTORNO_TELEPORT,
					Properties.EnumMetodo.URL_POSTUSERDATAREST);

			userUrlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					Properties.EnumEntorno.ENTORNO_TELEPORT,
					Properties.EnumMetodo.URL_POSTUSERDATAREST,
					Properties.EnumParam.USER_URL_POSTUSERDATAREST);

			passUrlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					Properties.EnumEntorno.ENTORNO_TELEPORT,
					Properties.EnumMetodo.URL_POSTUSERDATAREST,
					Properties.EnumParam.PASS_URL_POSTUSERDATAREST);

			urlGetStops = this.getData(Properties.EnumSistema.TELEPORT,
					Properties.EnumEntorno.ENTORNO_TELEPORT,
					Properties.EnumMetodo.URL_GETSTOPS);
		}
		else{
			urlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					envTeleport,
					Properties.EnumMetodo.URL_POSTUSERDATAREST);

			userUrlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					envTeleport,
					Properties.EnumMetodo.URL_POSTUSERDATAREST,
					Properties.EnumParam.USER_URL_POSTUSERDATAREST);

			passUrlPostUserDataRest = this.getData(Properties.EnumSistema.TELEPORT,
					envTeleport,
					Properties.EnumMetodo.URL_POSTUSERDATAREST,
					Properties.EnumParam.PASS_URL_POSTUSERDATAREST);

			urlGetStops = this.getData(Properties.EnumSistema.TELEPORT,
					envTeleport,
					Properties.EnumMetodo.URL_GETSTOPS);
		}
		vars.put(Properties.EnumMetodo.URL_POSTUSERDATAREST.getMetodo(), urlPostUserDataRest);
		vars.put(Properties.EnumParam.USER_URL_POSTUSERDATAREST.getParam(), userUrlPostUserDataRest);
		vars.put(Properties.EnumParam.PASS_URL_POSTUSERDATAREST.getParam(), passUrlPostUserDataRest);
		vars.put(Properties.EnumMetodo.URL_GETSTOPS.getMetodo(), urlGetStops);

		if(StringUtils.isEmpty(envSlcd)) {
			urlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					Properties.EnumEntorno.ENTORNO_SLCD,
					Properties.EnumMetodo.URL_POSTTOKENUSER);

			userUrlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					Properties.EnumEntorno.ENTORNO_SLCD,
					Properties.EnumMetodo.URL_POSTTOKENUSER,
					Properties.EnumParam.USER_URL_POSTTOKENUSER);

			passUrlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					Properties.EnumEntorno.ENTORNO_SLCD,
					Properties.EnumMetodo.URL_POSTTOKENUSER,
					Properties.EnumParam.PASS_URL_POSTTOKENUSER);

			urlFindVoyage = this.getData(Properties.EnumSistema.SLCD,
					Properties.EnumEntorno.ENTORNO_SLCD,
					Properties.EnumMetodo.URL_FINDVOYAGE);

			urlPostNewVoyage = this.getData(Properties.EnumSistema.SLCD,
					Properties.EnumEntorno.ENTORNO_SLCD,
					Properties.EnumMetodo.URL_POSTNEWVOYAGE);
		}
		else{
			urlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					envSlcd,
					Properties.EnumMetodo.URL_POSTTOKENUSER);

			userUrlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					envSlcd,
					Properties.EnumMetodo.URL_POSTTOKENUSER,
					Properties.EnumParam.USER_URL_POSTTOKENUSER);

			passUrlPostTokenUser = this.getData(Properties.EnumSistema.SLCD,
					envSlcd,
					Properties.EnumMetodo.URL_POSTTOKENUSER,
					Properties.EnumParam.PASS_URL_POSTTOKENUSER);

			urlFindVoyage = this.getData(Properties.EnumSistema.SLCD,
					envSlcd,
					Properties.EnumMetodo.URL_FINDVOYAGE);

			urlPostNewVoyage = this.getData(Properties.EnumSistema.SLCD,
					envSlcd,
					Properties.EnumMetodo.URL_POSTNEWVOYAGE);
		}

		vars.put(Properties.EnumMetodo.URL_POSTTOKENUSER.getMetodo(), urlPostTokenUser);
		vars.put(Properties.EnumParam.USER_URL_POSTTOKENUSER.getParam(), userUrlPostTokenUser);
		vars.put(Properties.EnumParam.PASS_URL_POSTTOKENUSER.getParam(), passUrlPostTokenUser);
		vars.put(Properties.EnumMetodo.URL_FINDVOYAGE.getMetodo(), urlFindVoyage);
		vars.put(Properties.EnumMetodo.URL_POSTNEWVOYAGE.getMetodo(), urlPostNewVoyage);

	}

}
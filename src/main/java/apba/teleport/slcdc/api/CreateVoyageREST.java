package apba.teleport.slcdc.api;

import apba.teleport.slcdc.dto.*;
import apba.teleport.slcdc.services.api.SlcdRestService;
import apba.teleport.slcdc.services.api.TeleportRestService;
import apba.teleport.slcdc.utils.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("voyage")
public class CreateVoyageREST {

	@Autowired
	SlcdRestService slcdRestService;

	@Autowired
	TeleportRestService teleportRestService;

	private Logger logger = LoggerFactory.getLogger(CreateVoyageREST.class);
	
	@PostMapping(value="createvogayes")
	public ResponseEntity<String> createvogayes(@RequestBody CreateVoyageRestDTO createVoyageRestDTO) {

		Date dateIni = createVoyageRestDTO.getDateIni();
		Date dateEnd = createVoyageRestDTO.getDateEnd();
		String user = createVoyageRestDTO.getUser();
		String pass = createVoyageRestDTO.getPass();
		Integer days = createVoyageRestDTO.getDays();
		String envTeleport = createVoyageRestDTO.getEnvTeleport();
		String envSlcd = createVoyageRestDTO.getEnvSlcd();

		Long shipImo = null;
		String visitNumber = null;

		List<Date> datesEtd = this.fillDates(days, dateIni, dateEnd);

		// Obtener parámetros de configuración
		Properties properties = new Properties();
		properties.calculateVars(envTeleport, envSlcd);

		HashMap<String, String> vars = properties.getVars();
		String urlPostUserDataRest = vars.get(Properties.EnumMetodo.URL_POSTUSERDATAREST.getMetodo());
		String userUrlPostUserDataRest = vars.get(Properties.EnumParam.USER_URL_POSTUSERDATAREST.getParam());
		String passUrlPostUserDataRest = vars.get(Properties.EnumParam.PASS_URL_POSTUSERDATAREST.getParam());
		String urlPostTokenUser = vars.get(Properties.EnumMetodo.URL_POSTTOKENUSER.getMetodo());
		String userUrlPostTokenUser = vars.get(Properties.EnumParam.USER_URL_POSTTOKENUSER.getParam());
		String passUrlPostTokenUser = vars.get(Properties.EnumParam.PASS_URL_POSTTOKENUSER.getParam());
		String urlGetStops = vars.get(Properties.EnumMetodo.URL_GETSTOPS.getMetodo());
		String urlFindVoyage = vars.get(Properties.EnumMetodo.URL_FINDVOYAGE.getMetodo());
		String urlPostNewVoyage = vars.get(Properties.EnumMetodo.URL_POSTNEWVOYAGE.getMetodo());

		if(!StringUtils.isEmpty(user) && !StringUtils.isEmpty(pass)){
			userUrlPostTokenUser = user;
			passUrlPostTokenUser = pass;
		}

		// Fin parámetros de configuración

		UserDataRestDTO uDataRestDTO = new UserDataRestDTO();
		uDataRestDTO.setUserName(userUrlPostUserDataRest);
		uDataRestDTO.setPassword(passUrlPostUserDataRest);

		ApiUserRestDTO userRestDTO = new ApiUserRestDTO();
		userRestDTO.setUsername(userUrlPostTokenUser);
		userRestDTO.setPassword(passUrlPostTokenUser);

		UserDataRestDTO userDataRestDTO = teleportRestService.postUserDataRest(urlPostUserDataRest, uDataRestDTO);
		String tokenTeleport = userDataRestDTO.getToken();

		final String tokenSLCDC = slcdRestService.postTokenUser(urlPostTokenUser, userRestDTO);

		datesEtd.stream().forEach(stopEtd -> {
			List<StopRestDTO> listStops = teleportRestService.getStops(urlGetStops, tokenTeleport, shipImo, stopEtd, visitNumber);
			if (listStops != null)
				listStops.stream().forEach(
						stop ->
						{
							Object voyag = slcdRestService.findVoyage(urlFindVoyage, tokenSLCDC, stop);
							if (voyag != null)
								logger.info("Encontrado ".concat(String.valueOf(stop.getVisitNumber())));
							else {
								logger.info("No encontrado ");

								VoyageRestDTO voyageRestDTO = this.createVoyageRestDto(stop);

								String r = slcdRestService.postNewVoyage(urlPostNewVoyage, tokenSLCDC, voyageRestDTO);
								logger.info(r);
							}
						});
		});

		return ResponseEntity.ok("Create Vogayes Process ok.");
	}


	private List<Date> fillDates(Integer days, Date dateIni, Date dateEnd)
	{
		List<Date> datesEtd = new ArrayList<>();

		ZoneId defaultZoneId = ZoneId.systemDefault();
		if (days != null) {
			// Parámetro numero de días desde hoy
			LocalDate nextDate = LocalDate.now().plusDays(days);
			Date stopEtd = Date.from(nextDate.atStartOfDay(defaultZoneId).toInstant());
			datesEtd.add(stopEtd);
		}
		else if(dateIni!= null && dateEnd!= null){
			// Parámetro de fecha inicial y fecha final
			LocalDate start = dateIni.toInstant().atZone(defaultZoneId).toLocalDate();
			LocalDate end = dateEnd.toInstant().atZone(defaultZoneId).toLocalDate();
			for (LocalDate date = start; !date.isAfter(end); date = date.plusDays(1)) {
				Date stopEtd = Date.from(date.atStartOfDay(defaultZoneId).toInstant());
				datesEtd.add(stopEtd);
			}
		}
		else if(dateIni!= null){
			datesEtd.add(dateIni);
		}
		else{
			LocalDate nextDate = LocalDate.now().plusDays(7);
			Date stopEtd = Date.from(nextDate.atStartOfDay(defaultZoneId).toInstant());
			datesEtd.add(stopEtd);
		}

		return datesEtd;
	}

	private VoyageRestDTO createVoyageRestDto(StopRestDTO stop) 	{
		VoyageRestDTO voyageRestDTO = new VoyageRestDTO();
		voyageRestDTO.setConsolidator("TERMINAL");
		voyageRestDTO.setDateTimeETA(stop.getStopEta());
		if(stop.getVisitNumber() != null)
			voyageRestDTO.setNumber(Long.toString(stop.getVisitNumber().longValue()));
		else
			voyageRestDTO.setNumber("1234567");
		voyageRestDTO.setShipImo(stop.getShipImo());
		voyageRestDTO.setShippingCompany("ADDO");
		//voyageRestDTO.setVoyageStatus("SIN_OPERAR");
		//oyageRestDTO.setType("LoLo");

		return voyageRestDTO;
	}


}

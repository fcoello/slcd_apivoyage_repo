package apba.teleport.slcdc.services.api;

import apba.teleport.slcdc.dto.ApiUserRestDTO;
import apba.teleport.slcdc.dto.StopRestDTO;
import apba.teleport.slcdc.dto.VoyageRestDTO;

public interface SlcdRestService {

    String postTokenUser(String url, ApiUserRestDTO userDTO);
    Object findVoyage(String url, String token, StopRestDTO stopRestDTO);
    String postNewVoyage(String url, String token, VoyageRestDTO voyageRestDTO);
   
}

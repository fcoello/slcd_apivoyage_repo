package apba.teleport.slcdc.services.api;

import apba.teleport.slcdc.dto.StopRestDTO;
import apba.teleport.slcdc.dto.UserDataRestDTO;

import java.util.Date;
import java.util.List;

public interface TeleportRestService {

    UserDataRestDTO postUserDataRest(String url, UserDataRestDTO userDataRestDTO);
    List<StopRestDTO> getStops(String url, String token, Long shipImo, Date stopEta, String visitNumber);
   
}

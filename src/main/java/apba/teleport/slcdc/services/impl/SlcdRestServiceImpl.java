package apba.teleport.slcdc.services.impl;

import apba.teleport.slcdc.dto.ApiUserRestDTO;
import apba.teleport.slcdc.dto.StopRestDTO;
import apba.teleport.slcdc.dto.VoyageRestDTO;
import apba.teleport.slcdc.services.api.SlcdRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class SlcdRestServiceImpl implements SlcdRestService
{
    private static final String HEADER_ACCEPT = "Accept";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private Logger logger = LoggerFactory.getLogger(SlcdRestServiceImpl.class);

    @Override
    public String postTokenUser(String url, ApiUserRestDTO userDTO)
    {

        try {

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            HttpEntity<ApiUserRestDTO> entity = new HttpEntity<>(userDTO);

            ResponseEntity<String> result;

            logger.debug("POST Token User vía REST... {}", url);

            result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

            logger.debug(result.getStatusCode().toString());

            return result.getBody();

        } catch (Exception e) {
            logger.error("{}", e + " " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public Object findVoyage(String url, String token, StopRestDTO stopRestDTO)
    {

        try {

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            header.set(HEADER_AUTHORIZATION, token);

            HttpEntity<?> entity = new HttpEntity<>(header);

            ResponseEntity<Object> result;

            logger.debug("GET Find Voyages vía REST... {}", url);

            String urlCompleta = url.concat("?");
            urlCompleta = urlCompleta.concat("number=").concat(String.valueOf(stopRestDTO.getVisitNumber().longValue()));

            result = restTemplate.exchange(urlCompleta, HttpMethod.GET, entity, Object.class);

            logger.debug(result.getStatusCode().toString());

            return result.getBody();

        } catch (Exception e) {
            logger.error("{}", e + " " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public String postNewVoyage(String url, String token, VoyageRestDTO voyageRestDTO) {

        try {

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HEADER_AUTHORIZATION, token);

            HttpEntity<VoyageRestDTO> entity = new HttpEntity<>(voyageRestDTO, headers);

            logger.debug("POST New Voyage vía REST... {}", url);
            ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
            logger.debug("Result = {}", result.getStatusCode());

            return result.getBody();
        } catch (Exception e) {
            logger.error("{}", e + " " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
}

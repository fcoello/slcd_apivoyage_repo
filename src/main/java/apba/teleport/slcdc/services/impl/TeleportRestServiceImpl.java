package apba.teleport.slcdc.services.impl;

import apba.teleport.slcdc.dto.StopRestDTO;
import apba.teleport.slcdc.dto.UserDataRestDTO;
import apba.teleport.slcdc.services.api.TeleportRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class TeleportRestServiceImpl implements TeleportRestService
{
    private static final String HEADER_ACCEPT = "Accept";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private Logger logger = LoggerFactory.getLogger(TeleportRestServiceImpl.class);

    @Override
    public UserDataRestDTO postUserDataRest(String url, UserDataRestDTO userDataRestDTO) {

        try {

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<UserDataRestDTO> entity = new HttpEntity<>(userDataRestDTO);

            logger.debug("POST User vía REST... {}", url);
            logger.debug("POST User vía REST... {} {} {} {}", userDataRestDTO.getTeleportSession(), userDataRestDTO.getToken(), userDataRestDTO.getUserName(), userDataRestDTO.getPassword().isEmpty());
            ResponseEntity<UserDataRestDTO> result = restTemplate.exchange(url, HttpMethod.POST, entity, UserDataRestDTO.class);
            logger.debug("Result = {}", result.getStatusCode());

            return result.getBody();
        } catch (Exception e) {
            logger.error("{}", e + " " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public List<StopRestDTO> getStops(String url, String token, Long shipImo, Date stopEta, String visitNumber)
    {

        try {

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            header.set(HEADER_AUTHORIZATION, token);

            HttpEntity<?> entity = new HttpEntity<>(header);

            ResponseEntity<StopRestDTO[]> result;

            logger.debug("GET Stops vía REST... {}", url);

            if(shipImo == null && stopEta == null && (visitNumber == null || visitNumber.isEmpty())) {
                result = restTemplate.exchange(url, HttpMethod.GET, entity, StopRestDTO[].class);
            } else {
                String urlCompleta = url + "query/stop?";
                String separador = "";

                if(shipImo != null) {
                    String shipImoS = shipImo.toString();
                    urlCompleta = urlCompleta.concat("shipIMO=").concat(shipImoS);
                    separador="&";
                }

                if(stopEta != null) {
                    logger.debug("getStopsFromTeleport. ETA = {}", stopEta);
                    LocalDateTime localDateTime = stopEta.toInstant().atZone(ZoneId.of("Europe/Madrid")).toLocalDateTime();

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
                    String formattedDateTime = localDateTime.format(formatter);
                    logger.debug("getStopsFromTeleport Local ETA = {}. Format = {}", localDateTime, formattedDateTime);

                    urlCompleta = urlCompleta.concat(separador).concat("ETA=").concat(formattedDateTime);
                    separador="&";
                }

                if (visitNumber != null && !visitNumber.isEmpty()) {
                    urlCompleta = urlCompleta.concat(separador).concat("visitNumber=").concat(visitNumber);
                }

                result = restTemplate.exchange(urlCompleta, HttpMethod.GET, entity, StopRestDTO[].class);
            }

            logger.debug(result.getStatusCode().toString());

            return Arrays.asList(result.getBody());

        } catch (Exception e) {
            logger.error("{}", e + " " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

}
